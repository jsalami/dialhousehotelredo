<h3>Someone has made a reservation!</h3>
<div>
    Name: {{ $fullname }} <br/>
    Phone Number: {{ $phone_no }} <br/>
    E-mail Address: {{ $email_add }} <br/>
    Number of Guests: {{ $number_of_guests }} <br/>
    Date of Reservation: {{ $date_of_reservation }} <br/>
    Time of Reservation: {{ $time_of_reservation }}
</div>

<p>Sent via {{ $email_add }}</p>