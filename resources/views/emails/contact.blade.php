<h3>You have a new contact via the contact form!</h3>
<div>
    First Name: {{ $first_name }} <br/>
    Last Name: {{ $last_name }} <br/>
    E-mail Address: {{ $email }} <br/>
    Phone Number: {{ $phone }} <br/>
    Message: {{ $bodyMessage }}
</div>

<p>Sent via {{ $email }}</p>