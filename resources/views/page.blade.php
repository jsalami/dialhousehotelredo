<!DOCTYPE html>
<html lang="en">

@include('partials._head')
<body>

@include('partials._pageloader')
@include('partials._navbar')

@yield('content')
   
    
@include('partials._scrolltotop')   
    
@include('partials._footer')

@include('partials._bottom')

    
</body>

</html>

