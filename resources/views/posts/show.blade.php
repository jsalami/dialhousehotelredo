@extends('page')

@section('title', 'Events')


@section('content')
    <div class="event-banner">
        <img src="http://www.dialhousehotel.com/wp-content/uploads/2018/02/valentines.jpg" alt="" width="100%" class="img-responsive" style="min-height:160px;">
    </div>
    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12 image-grids">
                    <div class="text-center">
                       <center><img src="{{asset('images/logo_small.png')}}" class="img-responsive" alt='Heading Image' width='100px'/></center>
                        <h3>{{$post->title}}</h3>
                        <div class="separator"></div>
                        <p>{{$post->main_body}}</p>
                    </div>
                    <div class="separator"></div>
                    <div class="col-md-6 text-center">
                        <a href="{{$post->menu}}"><button type="submit" class="button button-simple mt-30">Menu</button></a>
                    </div>
                    <div class="col-md-6 text-center">
                        <a href="{{$post->book_now}}"><button type="submit" class="button button-simple mt-30">Book Now</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
