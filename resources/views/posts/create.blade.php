@extends('page')

@section('title', 'Create New Event')


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>Create New Event</h1>
                <div class="seperator"></div>
                {!! Form::open(array('route' => 'posts.store')) !!}
                    {{ Form::label('title', 'Title:')}}
                    {{ Form::text('title', null, array('class' => 'form-control')) }}
                    
                    {{ Form::label('available_from', 'Available From:')}}
                    {{ Form::text('available_from', null, array('class' => 'form-control')) }}
                    
                    {{ Form::label('main_body', 'Main Body:')}}
                    {{ Form::textarea('main_body', null, array('class' => 'form-control')) }}
                    
                    {{ Form::label('menu', 'Link to Menu (Optional):')}}
                    {{ Form::text('menu', null, array('class' => 'form-control')) }}
                    
                    {{ Form::label('book_now', 'Book Now Link (Optional):')}}
                    {{ Form::text('book_now', null, array('class' => 'form-control')) }}
                    
                    {{ Form::submit('Create Post', array('class' => 'btn btn-success btn-lg'))}}
                {!! Form::close() !!}
            </div>
        </div>
    </div> 

@endsection


