@extends('main')
@section('content')
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/07/dialhouse.jpg)"></div>
            <div class="swiper-slide" style="background-image:url(http://lorempixel.com/1000/1000/nightlife/2)"></div>
            <div class="swiper-slide" style="background-image:url(http://lorempixel.com/1000/1000/nightlife/3)"></div>
            <div class="swiper-slide" style="background-image:url(http://lorempixel.com/1000/1000/nightlife/4)"></div>
            <div class="swiper-slide" style="background-image:url(http://lorempixel.com/1000/1000/nightlife/5)"></div>
        </div>

        <div class="swiper-pagination swiper-pagination-white"></div>

        <div class="swiper-button-next swiper-button-white"></div>
        <div class="swiper-button-prev swiper-button-white"></div>
    </div>

    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12 image-grids">
                    <div class="main-heading text-center">
                       <center><img src="{{asset('images/logo_small.png')}}" class="img-responsive" alt='Heading Image' width='100px' /></center>
                        <h3>The Dial House</h3>
                        <div class="separator"></div>
                        <p>The Dial House Hotel is part of The Barons Eden Group collection. This comprises some highly individual properties, each with its own unmistakable character.</p>
                        <p>With each property we have created an ambience that is unhurried and relaxing. The closest sibling is Huxleys, an Italian cafe and restaurant in nearby Chipping Campden, which has a distinctive style and menu.</p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--logo-->
    
    <div class="friends-image-grids">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
                <div class="col-md-12"></div>
            </div>
        </div>
    </div>
    
    <!--visit website-->
    <div class="visit-website">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4>Visit Hoar Cross Hall</h4>
                    <div class="separator"></div>
                    <p>The hotel offers a range of spa breaks and spa days and is a much-loved location for weddings and business meetings. It welcomes visitors to its formal restaurant, the Ballroom, as well as the more casual atmosphere found in its Huxleys cafe.</p>
                </div>
            </div>
        </div>
    </div>
    
@endsection     