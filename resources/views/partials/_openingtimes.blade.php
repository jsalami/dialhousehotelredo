<div class="opening-times text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h3 class="opening-times-headings">Opening Times</h3>
                <div class="separator"></div>
                <p>We are open seven days a week</p>
                <div class="col-md-6 pt-20 pb-20">
                    <h4 class="opening-times-headings">Opening Hours</h4>
                    <p><b>Mon - Sun</b> 8.00am - 10.30pm</p>
                </div>
                <div class="col-md-6 pt-20 pb-20">
                    <h4 class="opening-times-headings">Food Served</h4>
                    <p><b>Mon - Sun</b> 8.00am - 10.30pm</p>
                </div>
            </div>
        </div>
    </div>
</div>