<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                     <div id="footerTitle">For all enquiries and to book Call</div>
                     <div id="footerNumber"><a href="tel:+441451822244">01451 822 244</a></div>
                     <div class="separator"></div>
                     <div id="footerContact"><a href="maitlto:info@dialhousehotel.com">info@dialhousehotel.com</a></div>
                     <div id="footerAddress">The Dial House Hotel, High Street, Bourton on the Water, GL54 2AN<br>© 2017 The Dial House  |  <a href="/terms-and-conditions/">Terms and Conditions</a>  |  <a href="/privacy-policy/">Privacy Policy</a> | <a href="/careers">Careers</a><br>Part of <a href="http://www.baronsedengroup.com">The Barons Eden Group</a></div>
                </div>
            </div>
        </div>
    </footer>