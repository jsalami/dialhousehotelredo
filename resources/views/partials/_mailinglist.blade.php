<div class="mailing-list">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 text-center">
                    <h3>Join our mailing list</h3>
                    <div class="separator"></div>
                    <p>We send out occasional emails, only when we have exceptional offers and news that we think you’ll be interested in. We will never use your data for any other purpose and it will never be shared with anyone else.</p>
                    <center>
                    <!-- Begin MailChimp Signup Form -->
                    <style type="text/css">
                        /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
                           We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                    </style>
                    <div id="mc_embed_signup">
                    <form action="https://dialhousehotel.us16.list-manage.com/subscribe/post?u=b02dacb60841911c8b3c3f4b2&amp;id=9bbf4f9ea4" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="novalidate">
                        <div id="mc_embed_signup_scroll">
                    <div class="mc-field-group">
                        <label for="mce-FNAME"> </label>
                        <input type="text" value="" name="FNAME" class="required form-text" id="mce-FNAME" placeholder="First Name" aria-required="true">
                    </div>
                    <div class="mc-field-group">
                        <label for="mce-LNAME"></label>
                        <input type="text" value="" name="LNAME" class="required" id="mce-LNAME" placeholder="Last Name" aria-required="true">
                    </div>
                    <div class="mc-field-group">
                        <label for="mce-EMAIL"> </label>
                        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="E-mail Address" aria-required="true">
                    </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_b02dacb60841911c8b3c3f4b2_9bbf4f9ea4" tabindex="-1" value=""></div>
                        <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button button-simple mt-30"></div>
                        </div>
                    </form>
                    </div>
                    <script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js"></script><script type="text/javascript">(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                    <!--End mc_embed_signup-->
                    </center>
                </div>
            </div>
        </div>
    </div>