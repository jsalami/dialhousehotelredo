<div class="reservation-enquiry">
        <div class="container">
            <div class="row">
                <h3 class='text-center'>Reservation Enquiry</h3>
                <div class="separator"></div>
                 <div class="email-success-message"></div>
                  <form class="reservation-form" novalidate="novalidate" id="resForm">
                   {{ csrf_field() }}
                    <div class="primary-form">
                        <div class="col-md-6">
                            <span class="wpcf7-form-control-wrap text-768"><input type="text" name="fullname" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Name" required/></span>
                        </div>
                        <div class="col-md-6">
                            <span class="wpcf7-form-control-wrap email-766"><input type="email" name="email_add" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-mail address" required/></span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <span class="wpcf7-form-control-wrap phone"><input type="text" name="phone_no" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Contact Number" required/></span>
                        </div>
                        <div class="col-md-6">
                            <span class="wpcf7-form-control-wrap phone"><select class="form-control" id="sel1" name="number_of_guests" required>
                               <option value="" selected disabled>-- Number of guests --</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8+</option>
                              </select></span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <span class="wpcf7-form-control-wrap phone"><input type="text" name="date" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Date required" required/></span>
                        </div>
                        <div class="col-md-6">
                            <span class="wpcf7-form-control-wrap phone"><input type="text" name="time" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Preferred time" required/></span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 text-center">
                           <br>
                            <center><div class="g-recaptcha" data-sitekey="6LfDLkgUAAAAAJrw4K4X-lEkz0tOsUz6obeGCRv7"></div></center>
                            <button type="submit" class="button button-simple mt-30">Send Message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>