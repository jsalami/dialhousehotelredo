<div class="contactus-form pb-60">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                          <div class="vc_empty_space" style="height: 55px"><span class="vc_empty_space_inner"></span></div>
                           <div class="email-success-messge"></div>
                            <form class="wpcf7-form" id="form-id">
                               {{ csrf_field() }}
                                <div class="primary-form">
                                    <div class="col-xs-6">
                                        <span class="wpcf7-form-control-wrap text-768"><input type="text" name="first-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" placeholder="First Name" required /></span>
                                    </div>
                                    <div class="col-xs-6">
                                        <span class="wpcf7-form-control-wrap text-768"><input type="text" name="last-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Last Name" required /></span>
                                    </div>
                                    <div class="col-xs-6">
                                        <span class="wpcf7-form-control-wrap email-766"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-mail Address" required/></span>
                                    </div>
                                    <div class="col-xs-6">
                                        <span class="wpcf7-form-control-wrap phone"><input type="text" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Phone Number" required /></span>
                                    </div>
                                    <div class="col-xs-12">
                                        <span class="wpcf7-form-control-wrap textarea-6"><textarea name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Your Message" required ></textarea></span>
                                    </div>
                                    
                                    <div class="col-xs-12 mt-10 text-center">
                                       <p class="clearfix"></p>
                                            <center><div class="g-recaptcha" data-sitekey="6LfDLkgUAAAAAJrw4K4X-lEkz0tOsUz6obeGCRv7"></div>
                                            <button type="submit" class="button button-simple btn-submit mt-30">SEND</button></center>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    