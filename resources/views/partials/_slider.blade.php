<div class="swiper-container">
    <div class="swiper-wrapper">
        @yield('slide-image')
    </div>

    <div class="swiper-pagination swiper-pagination-white"></div>
</div>