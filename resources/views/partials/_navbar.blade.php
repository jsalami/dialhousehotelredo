    <div id="top-bar" class="hidden-xs">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <ul class="top-bar-info top-icons-section">
                        <li><a href="mailto:info@dialhousehotel.com" target="_blank"><i class="fa fa-envelope fa-5x"></i></a></li>
                        <li><a href="https://twitter.com/dialhouse" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.instagram.com/dialhousebourton/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://en-gb.facebook.com/thedialhousehotel/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.google.co.uk/maps/place/Dial+House+Hotel/@51.8847546,-1.7588772,17z/data=!3m1!4b1!4m5!3m4!1s0x4871259cfef00179:0xee04afa57bc09f26!8m2!3d51.8847546!4d-1.7566885" target="_blank"><i class="fa fa-map-o" aria-hidden="true"></i></a></li>
                        <li><a href="tel:+441451822244" class="telephone-number">01451 822 244</a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-md-offset-5 col-xs-12">
                    <ul class="top-bar-info pull-right">
                        <li><a href="/dine#resForm" class="goldBorder">Book a Table</a></li>
                        <li><a href="https://app.thebookingbutton.com/properties/dialhousedirect?check_in_date=05-03-2018&check_out_date=06-03-2018&number_adults=2" class="goldBorder" target="_blank">Book a Room</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
   
    <header>
        <nav class="navbar navbar-default navbar-custom nav-inline" data-spy="affix" data-offset-top="50">
            <div class="container">
                <div class="row">
                   <center><img src="{{asset('images/dhh_logo_new.svg')}}" class="img-responsive big-navbar-logo" alt='Dial House Logo' width='270px' /></center> 
                    <div class="navbar-header navbar-header-custom">
                     <a class="navbar-brand small-navbar-logo" href="/"><img src="{{asset('images/dhh_logo_new.svg')}}" class="img-responsive" alt='Dial House Logo' width='200px' /></a>
                      <button type="button" class="navbar-toggle offcanvas-toggle pull-right" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas" style="float:left;">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                       
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul id="menu-main-menu" class="nav navbar-nav navbar-right navbar-links-custom">
                            <li id="menu-item-46" class="{{ Request::is('/') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/">The Hotel<span></span></a></li>
                            <li id="menu-item-45" class="{{ Request::is('dine') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}} "><a href="/dine">Dine<span></span></a></li>
                            <li id="menu-item-44" class="{{ Request::is('rooms') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}} "><a href="/rooms">
                                Rooms<span></span></a></li>
                            <li id="menu-item-44" class="{{ Request::is('bookings') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}} "><a href="/bookings">Bookings<span></span></a></li>
                            <li id="menu-item-44" class="{{ Request::is('weddings') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/weddings">Weddings<span></span></a></li>
                            <li id="menu-item-44" class="{{ Request::is('events') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/events">Events<span></span></a></li>
                            <li id="menu-item-243" class="{{ Request::is('friends') ? 'active menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-243 dropdown submenu': 'menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-243 dropdown submenu'}}"><a href="/friends">Friends <i class="fa fa-angle-down" style="font-size:14px;"></i><span></span></a>
                            <ul class="dropdown-menu">
                                <li id="menu-item-241" class="{{ Request::is('friends/hoarcrosshall') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/friends/hoarcrosshall">Hoar Cross Hall</a></li>
                                <li id="menu-item-244" class="{{ Request::is('friends/eden-hall') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/friends/eden-hall">Eden Hall</a></li>
                                <li id="menu-item-244" class="{{ Request::is('friends/huxleys') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/friends/huxleys">Huxleys</a></li>
                            </ul>
                            </li>
                            
                            <li id="menu-item-44" class="{{ Request::is('contact') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/contact">Contact<span></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        
        <div class="navbar-offcanvas navbar-offcanvas-touch navbar-offcanvas-right" id="js-bootstrap-offcanvas">
            <button class="closing-menu-button"><i class="fa fa-remove" style="font-size:20px;background:rgba(0,0,0,0.4);padding:5px 6px;color:#fff;" class="closing-menu-button" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas"></i></button>
            <ul class="nav navbar-nav">
                <li class="{{ Request::is('/') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/">The Hotel</a></li>
                <li class="{{ Request::is('dine') ? 'active menu-item': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/dine">Dine</a></li>
                <li class="{{ Request::is('rooms') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/rooms">Rooms</a></li>
                <li class="{{ Request::is('bookings') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/bookings">Bookings</a></li>
                <li class="{{ Request::is('weddings') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/weddings">Weddings</a></li>
                <li class="{{ Request::is('events') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/events">Events</a></li>
                <li class="dropdown">
                    <a href="/friends" class="{{ Request::is('friends') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45 dropdown-toggle': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45 dropdown-toggle'}}">Friends <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li class="{{ Request::is('friends/hoarcrosshall') ? ' active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/friends/hoarcrosshall">Hoar Cross Hall</a></li>
                        <li class="{{ Request::is('friends/eden-hall') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/friends/eden-hall">Eden Hall</a></li>
                        <li class="{{ Request::is('friends/huxleys') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/friends/huxleys">Huxleys</a></li>
                    </ul>
                </li>
                <li class="{{ Request::is('contact') ? 'active menu-item menu-item-type-post_type menu-item-object-page menu-item-45': 'menu-item menu-item-type-post_type menu-item-object-page menu-item-45'}}"><a href="/contact">Contact</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <center><li><a href="#"><button type="submit" class="button button-simple mt-30">Book a Table</button></a></li></center>
                <center><li><a href="#"><button type="submit" class="button button-simple mt-30">Book a Room</button></a></li></center>
            </ul>
        </div>
    </header>
    
    