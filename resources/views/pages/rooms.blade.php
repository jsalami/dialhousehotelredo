@extends('main') @section('title', 'Rooms')
@section('slide-image')

    <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/08/shutterstock_461151403-edit.jpg)"></div>

@endsection
 
   @section('content')


<div class="intro">
    <div class="container">
        <div class="row">
            <div class="col-md-12 image-grids">
                <div class="text-center">
                    <center><img src="{{asset('images/logo_small.png')}}" class="img-responsive" alt='Heading Image' width='100px' /></center>
                    <h3>Rooms</h3>
                    <div class="separator"></div>
                    <p>Part of the charm of the hotel lies in the wonderful individuality of our 13 bedrooms; from stunning village and river views in our Awesome rooms, through snug double beds in our Cosy rooms. Rooms are situated within the main house and our adjoining Coach House.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="rooms-body">
    <div class="container">
        <div class="row">
            <div class="col-md-6"><img src="http://www.dialhousehotel.com/wp-content/uploads/2017/09/room-5.jpg" alt="Picture of room one" class="img-responsive" width="100%" height="300px"></div>
            <div class="col-md-6">
                <div class="rooms-photo-text">
                    <h2><span style="color: #bb9b50;">Cosy<br>
</span></h2>
                    <p>Our Cosy double rooms are small but perfectly appointed for your stay. Each one has a double bed and views out to the garden from the main house.</p>
                    <div class="wideSeparatorDark"></div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 col-md-push-6"><img src="{{asset('images/splendid-500x300.png')}}" alt="Picture of room two" class="img-responsive" width="100%" height="300px"></div>
            <div class="col-md-6 col-md-pull-6">
                <div class="rooms-photo-text">
                    <h2><span style="color: #bb9b50;">Splendid<br>
</span></h2>
                    <p>Our Splendid rooms are more spacious than the Cosy and located in both the main house and the coach house building. Beautifully appointed with Cotswolds charm.</p>
                    <div class="wideSeparatorDark"></div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6"><img src="{{asset('images/awesome-500x300.png')}}" alt="Picture of room three" class="img-responsive" width="100%" height="300px"></div>
            <div class="col-md-6">
                <div class="rooms-photo-text">
                    <h2><span style="color: #bb9b50;">Awesome<br>
</span></h2>
                    <p>Our Awesome rooms are wonderfully comfortable, similar to the Splendid but with stunning views of the village and River Windrush. These truly sumptuous rooms are the perfect place to relax and call ‘home from home’.</p>
                    <div class="wideSeparatorDark"></div>
                </div>
            </div>
        </div>
    </div>
    
</div>

<div class="room-info">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center pt-60 pb-60 ">
                <p>Room amenities include, flat screen TV, tea and coffee making facilities.<br> Free wi-fi is available throughout the hotel.<br> Please let us know if you have any special requirements when booking.</p>
                <button type="submit" class="button button-simple mt-30"> Book a Room</button>
            </div>
        </div>
    </div>
</div>




@endsection