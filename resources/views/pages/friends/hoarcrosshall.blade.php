@extends('main')
@section('title', 'Hoar Cross Hall')
@section('slide-image')

    <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/08/HCH-Day-1-APR17-WEBres-66.jpg)"></div>

@endsection
@section('content')
    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12 image-grids">
                    <div class="text-center">
                       <center><img src="{{asset('images/logo_small.png')}}" class="img-responsive" alt='Heading Image' width='100px' /></center>
                        <h3>Hoar Cross Hall</h3>
                        <div class="separator"></div>
                        <p>Set in 50 acres of formal gardens and grounds, on the edge of the National Forest near Burton on Trent, Hoar Cross Hall is a Grade II listed property, built in the 1860’s by Sir Hugo Meynell Ingram as a home for his family. </p>
                        <p>The hotel is renowned for comfort and tranquility, with a style that effortlessly blends a modern feel with the grandeur of the old hall. Its spa boasts a dazzling array of facilities.</p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--logo-->
    <center><img src="http://www.dialhousehotel.com/wp-content/uploads/2017/08/hoarcross_logo.png" alt="Hoar Cross Hall Logo" width="250px"></center>
    <br>
    <div class="friends-image-grids">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4 mt-20"><img src="http://www.dialhousehotel.com/wp-content/uploads/2017/08/HCH-Day-2-APR17-WEBres-49-e1503265948125.jpg" alt="" class="img-responsive"></div>
                <div class="col-md-4 col-sm-4 col-xs-4 mt-20"><img src="{{asset('images/HCH-imagegrid-2.png')}}" alt="" class="img-responsive" width="100%"></div>
                <div class="col-md-4 col-sm-4 col-xs-4 mt-20 "><img src="{{asset('images/HCH-imagegrid-1.png')}}" alt="" class="img-responsive" width="100%"></div>
                <div class="col-md-12 col-sm-12 col-xs-12 mt-30">
                    <img src="{{asset('images/HCH-longgrid-img.png')}}" alt="" class="img-responsive" width="100%">
                </div>
            </div>
        </div>
    </div>
    
    <!--visit website-->
    <div class="visit-website">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h4>Visit Hoar Cross Hall</h4>
                    <div class="separator"></div>
                    <p>The hotel offers a range of spa breaks and spa days and is a much-loved location for weddings and business meetings. It welcomes visitors to its formal restaurant, the Ballroom, as well as the more casual atmosphere found in its Huxleys cafe.</p>
                    <button type="submit" class="button button-simple mt-30">Find Out More</button>
                </div>
            </div>
        </div>
    </div>
    
@endsection     