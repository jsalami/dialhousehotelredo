@extends('main')
@section('title', 'Eden Hall')
@section('slide-image')
        <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/09/The-Dial-House-website-Eden-09-1.jpg)"></div>
        <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/09/The-Dial-House-website-Eden-08.jpg)"></div>
        <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/09/The-Dial-House-website-Eden-03.jpg)"></div>
    @endsection
@section('content')

    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12 image-grids">
                    <div class="text-center">
                       <center><img src="{{asset('images/logo_small.png')}}" class="img-responsive" alt='Heading Image' width='100px' /></center>
                        <h3>Eden Hall Day Spa</h3>
                        <div class="separator"></div>
                        <p>Eden Hall was built by a firm of London contractors for Robert Middleton, a farmer from Newark-on-Trent in Nottinghamshire.  The work commenced in 1872 and this memorable building was completed in 1875. </p>
                        <p>Eden is now an award-winning spa, renowned for providing guests with an experience that is highly relaxing and good for the soul.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--logo-->
    <center><img src="http://www.dialhousehotel.com/wp-content/uploads/2017/09/EdenHall.png" alt="Eden Hall Day Spa Logo" width="250px"></center>
    <br>
    <div class="friends-image-grids">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4 mt-20"><img src="{{asset('images/EHS-imagegrid-1.png')}}" alt="" class="img-responsive" width="100%"></div>
                <div class="col-md-4 col-sm-4 col-xs-4 mt-20"><img src="{{asset('images/EHS-imagegrid-2.png')}}" alt="" class="img-responsive" width="100%"></div>
                <div class="col-md-4 col-sm-4 col-xs-4 mt-20"><img src="{{asset('images/EHS-imagegrid-3.png')}}" alt="" class="img-responsive" width="100%"></div>
                <div class="col-md-12 col-sm-12 col-xs-12 mt-30">
                    <img src="{{asset('images/EHS-longgrid-img.png')}}" alt="" class="img-responsive" width="100%">
                </div>
            </div>
        </div>
    </div>
    
    <!--visit website-->
    <div class="visit-website">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h4>Visit Eden Hall</h4>
                    <div class="separator"></div>
                    <p>Eden offers a number of different spa days and a wide range of treatments by its highly skilled therapists. Bookings can be made either on-line or over the telephone.</p>
                    <button type="submit" class="button button-simple mt-30">Find Out More</button>
                </div>
            </div>
        </div>
    </div>
    
@endsection     