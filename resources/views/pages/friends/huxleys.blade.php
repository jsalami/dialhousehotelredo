@extends('main')
@section('title', 'Huxleys')
@section('slide-image')
        <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/09/The-Dial-House-website-cover2-03.jpg)"></div>
        <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/09/The-Dial-House-website-Huxleys-03-1.png)"></div>
        <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/09/The-Dial-House-website-cover1-03.jpg)"></div>
    @endsection
@section('content')
    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12 image-grids">
                    <div class="text-center">
                       <center><img src="{{asset('images/logo_small.png')}}" class="img-responsive" alt='Heading Image' width='100px' /></center>
                        <h3>Huxleys</h3>
                        <div class="separator"></div>
                        <p>Located in Chipping Campden, a picture-perfect town in the Cotswolds, Huxleys occupies a breathtaking seventeenth century building. </p>
                        <p>It is a cafe and restaurant which pairs old English charm with Italian flair. In addition to serving tea, coffee and cakes, it also has a full menu of Italian-inspired dishes.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--logo-->
    <center><img src="http://www.dialhousehotel.com/wp-content/uploads/2017/09/Huxleys.png" alt="Huxleys Logo" width="250px"></center>
    <br>
    <div class="friends-image-grids">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4 mt-20"><img src="{{asset('images/H-imagegrid-1.png')}}" alt="" class="img-responsive" width="100%"></div>
                <div class="col-md-4 col-sm-4 col-xs-4 mt-20"><img src="{{asset('images/H-imagegrid-2.png')}}" alt="" class="img-responsive" width="100%"></div>
                <div class="col-md-4 col-sm-4 col-xs-4 mt-20"><img src="{{asset('images/H-imagegrid-3.png')}}" alt="" class="img-responsive" width="100%"></div>
                <div class="col-md-12 col-sm-12 col-xs-12 mt-30">
                    <img src="{{asset('images/H-longgrid-img.png')}}" alt="" class="img-responsive" width="100%">
                </div>
            </div>
        </div>
    </div>
    
    <!--visit website-->
    <div class="visit-website">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h4>Visit Huxleys</h4>
                    <div class="separator"></div>
                    <p>The cafe spread its wings in 2017 with a friendly takeover of the adjacent bank. This added a first floor terrace with sweeping views of Market Square as well as two private rooms which can be hired for private functions.</p>
                    <button type="submit" class="button button-simple mt-30">Find Out More</button>
                </div>
            </div>
        </div>
    </div>
    
@endsection     