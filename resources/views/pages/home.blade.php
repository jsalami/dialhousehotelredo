@extends('main')

@section('title', 'Dialhouse Hotel')
@section('slide-image')

    <div class="swiper-slide" style='background-image:url(http://dialhousehotel.test/wp-content/uploads/2017/07/dialhouse.jpg)'></div>

@endsection
@section('content')

    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12 image-grids">
                    <div class="text-center">
                        <center><img src="{{asset('images/logo_small.png')}}" class="img-responsive" alt='Heading Image' width='100px' /></center>
                        <h3>The Dial House</h3>
                        <div class="separator"></div>
                        <p>With its enviable position just a stone's throw off the charming High Street of Bourton on the Water and the calming waters of the River Windrush, The Dial House offers you a unique way to enjoy life in the Cotswolds. </p>
                        <p>Built in the world-renowned style known as the Costwold Venacular, the building dates back to 1698 and is one of the village’s elder statesmen. It has a wonderful character; deep-set stone walls, cosy corners, fireplaces and an individuality that becomes a home from home before you know it.</p>
                        <p>Enjoy a wonderfully traditional Bed &amp; Breakfast, take coffee and afternoon tea on our front terrace or back garden, taste our Italian-inspired menu, but above all, enjoy a memory-filled time nestled deep within the Cotswolds.</p>
                    </div>
                </div>

                <div class="">
                    <div class="col-md-4 col-sm-6 col-xs-12 isotope-item business marketing">
                        <a href="/dine">
                            <div class="project-item">
                                <div class="overlay-container">
                                    <img src="http://dialhousehotel.com/wp-content/uploads/2017/07/1.jpg" alt="project-1">
                                    <div class="project-item-overlay">
                                        <h4>Dine</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 isotope-item business financial">
                        <a href="/rooms">
                            <div class="project-item">
                                <div class="overlay-container">
                                    <img src="http://dialhousehotel.com/wp-content/uploads/2017/08/splendid-750x750.jpg" alt="project-1">
                                    <div class="project-item-overlay">
                                        <h4>Rooms</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 isotope-item marketing">
                        <a href="/bookings">
                            <div class="project-item">
                                <div class="overlay-container">
                                    <img src="http://www.dialhousehotel.com/wp-content/uploads/2017/07/3.jpg" alt="project-1">
                                    <div class="project-item-overlay">
                                        <h4>Book</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 isotope-item business">
                        <a href="/friends">
                            <div class="project-item">
                                <div class="overlay-container">
                                    <img src="http://dialhousehotel.com/wp-content/uploads/2017/07/7.jpg">
                                    <div class="project-item-overlay">
                                        <h4>Friends</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 isotope-item financial marketing">
                        <a href="/events">
                            <div class="project-item">
                                <div class="overlay-container">
                                    <img src="http://dialhousehotel.com/wp-content/uploads/2017/07/5.jpg">
                                    <div class="project-item-overlay">
                                        <h4>Events</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 isotope-item business financial marketing">
                        <a href="/contact">
                            <div class="project-item">
                                <div class="overlay-container">
                                    <img src="http://dialhousehotel.com/wp-content/uploads/2017/07/9.jpg" alt="project-1">
                                    <div class="project-item-overlay">
                                        <h4>Contact</h4>
                                        <p >We are open seven days a week.<br/> We’d love to hear from you. </p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    
    @include('partials._openingtimes')
    @include('partials._googlemaps')

@endsection     