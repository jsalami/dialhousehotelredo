@extends('main')
@section('title', 'Events')
@section('slide-image')

    <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2018/01/shutterstock_492017866.jpg)"></div>

@endsection
@section('content')

    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12 image-grids">
                    <div class="text-center">
                       <center><img src="{{asset('images/logo_small.png')}}" class="img-responsive" alt='Heading Image' width='100px' /></center>
                        <h3>Events</h3>
                        <div class="separator"></div>
                        <p>The Dial House Hotel has become host to a variety of must see special events. Our charming Cotswold hotel, fabulous food and impeccable service make The Dial house a truly unique location for any event.</p>
                        <p>View our upcoming events below.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="events mb-100">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                   <a href="/events/valentines-offer">
                    <div class="single-event text-center">
                        <img src="http://www.dialhousehotel.com/wp-content/uploads/2018/01/CC_1419-The-Dial-House-website-01-18-04.jpg" alt="" style="border:2px solid #bb9b50;">
                        <h2>Valentine's Offer</h2>
                        <p>Available from the 10th to 17th February</p>
                        <div class="separator"></div>
                        <button type="submit" class="button button-simple mt-30">Read More</button>
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    
@endsection     