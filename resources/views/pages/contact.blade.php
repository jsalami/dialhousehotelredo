@extends('main')
@section('title', 'Contact')
@section('slide-image')

    <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/08/shutterstock_461151403-edit.jpg)"></div>

@endsection
@section('content')
    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12 image-grids">
                    <div class="text-center">
                       <center><img src="{{asset('images/logo_small.png')}}" class="img-responsive" alt='Heading Image' width='100px' /></center>
                        <h3>The Dial House</h3>
                        <div class="separator"></div>
                        <p>For all enquiries and to book call</p>
                        <h2>01451 822 244</h2>
                        <p>We look forward to hearing from you.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @include('partials._contactform')
    <img src="http://www.dialhousehotel.com/wp-content/uploads/2017/08/Dialhouse-May17-WEBres-5_v2.jpg" alt="A picture of The Dial House" class="img-responsive" width="100%">
    <div class="find-us">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>The Dial House</h3>
                    <div class="separator"></div>
                    <p>The Dial House, High Street, Bourton on the Water, GL54 2AN</p>
                    <p>The Dial House is located right in the heart of the village of Bourton on the Water.
Click the button below to view The Dial House on Google Maps.</p>
                   <button type="submit" class="button button-simple mt-30">View Map</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="gold-background">
        @include('partials._mailinglist')
    </div>
    
    @include('partials._openingtimes')
@endsection     