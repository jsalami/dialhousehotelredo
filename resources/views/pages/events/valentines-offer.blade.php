@extends('page')
@section('title', 'Valentine’s Offer')
@section('content')
    <div class="event-banner">
        <img src="http://www.dialhousehotel.com/wp-content/uploads/2018/02/valentines.jpg" alt="" width="100%" class="img-responsive" style="min-height:160px;">
    </div>

    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12 image-grids">
                    <div class="text-center">
                       <center><img src="{{asset('images/logo_small.png')}}" class="img-responsive" alt='Heading Image' width='100px'/></center>
                        <h3>Valentine’s Offer</h3>
                        <div class="separator"></div>
                        <p><b>Have you made plans for Valentine’s Day yet?</b></p>
                        <p>Why not book a mini break in the Cotswolds? Imagine this… Pull up to our beautiful hotel on a crisp winter’s morning, check in to your cosy bedroom and head straight out to explore the picturesque streets of Bourton-on-the Water.</p>
                        <p>Enjoy a romantic 3-course meal that evening, stay the night and enjoy an award-winning breakfast the next morning.</p>
                        <br>
                        <p><b>Dinner, bed and breakfast packages start from £220(based on 2 people staying).</b></p>
                        <p>Available from 10th to 17th February. This offer has limited availability, book now to secure your booking.</p>
                    </div>
                    <div class="separator"></div>
                    <div class="col-md-6 text-center">
                        <button type="submit" class="button button-simple mt-30">Menu</button>
                    </div>
                    <div class="col-md-6 text-center">
                        <button type="submit" class="button button-simple mt-30">Book Now</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection     