@extends('main')
@section('title', 'Weddings')
@section('slide-image')

    <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/12/1920x1080-TDH-02.jpg)"></div>

@endsection
@section('content')
    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12 image-grids">
                    <div class="text-center">
                       <center><img src="{{asset('images/logo_small.png')}}" class="img-responsive" alt='Heading Image' width='100px' /></center>
                        <h3>Your wedding in the Cotswolds</h3>
                        <div class="separator"></div>
                        <p>You can have your special day in the gardens of one of the oldest buildings in Bourton-on-the-Water. With the backdrop of our charming Cotswolds hotel for those essential photos of the big day.</p>
                        <p>
We can cater for up to 80 guests for your wedding breakfast, in your choice of a marquee, teepee or yurt and can host up to 120 guests in the evening.</p>
                      <br>
                       <button type="submit" class="button button-simple">Download our brochure</button>
                       <br><br>
                        <p>For more information give us a call on 01451 822 244 or leave us your details below and we will be in touch.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @include('partials._contactform')
        
@endsection     