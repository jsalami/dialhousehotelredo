@extends('main')
@section('title', 'Bookings')
@section('slide-image')

    <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/08/shutterstock_451156918.jpg)"></div>
    <div class="swiper-slide" style='background-image:url(http://dialhousehotel.test/wp-content/uploads/2017/07/dialhouse.jpg)'></div>
@endsection     

@section('content')
    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12 image-grids">
                    <div class="main-heading text-center">
                       <center><img src="{{asset('images/logo_small.png')}}" class="img-responsive" alt='Heading Image' width='100px' /></center>
                        <h3>Bookings</h3>
                        <div class="separator"></div>
                        <button type="submit" class="button button-simple mt-10"> Book a Room</button>
                        <p>For all enquiries and to book call</p>
                        <h2>01451 822 244</h2>
                        <p>Alternatively, please fill in the contact form shown below and we’ll be in touch with you shortly.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    @include('partials._contactform')
    @include('partials._mailinglist')
    
@endsection         