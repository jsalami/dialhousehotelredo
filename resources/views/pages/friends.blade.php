@extends('main')
@section('title', 'Friends')
@section('slide-image')

    <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/07/dialhouse.jpg)"></div>

@endsection
@section('content')
    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12 image-grids">
                    <div class="text-center">
                       <center><img src="{{asset('images/logo_small.png')}}" class="img-responsive" alt='Heading Image' width='100px' /></center>
                        <h3>The Dial House</h3>
                        <div class="separator"></div>
                        <p>The Dial House Hotel is part of The Barons Eden Group collection. This comprises some highly individual properties, each with its own unmistakable character.</p>
                        <p>With each property we have created an ambience that is unhurried and relaxing. The closest sibling is Huxleys, an Italian cafe and restaurant in nearby Chipping Campden, which has a distinctive style and menu.</p>
                        <p>We understand that when you choose to visit a hotel, a day spa, a cafe or restaurant, you do not make that choice lightly. It would be an absolute pleasure to host you at any of our properties.</p>
                    </div>
                    <div class="">
                    <div class="col-md-4 col-sm-6 col-xs-12 isotope-item business marketing">
                        <a href="/friends/hoarcrosshall">
                            <div class="project-item">
                                <div class="overlay-container">
                                    <img src="http://www.dialhousehotel.com/wp-content/uploads/2017/08/HCH-Day-1-APR17-WEBres-68.jpg" alt="project-1" heigth="560px" width="360px">
                                    <div class="project-item-overlay">
                                        <h4>Hoar Cross Hall Hotel</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 isotope-item business financial">
                        <a href="/friends/eden-hall">
                            <div class="project-item">
                                <div class="overlay-container">
                                    <img src="http://www.dialhousehotel.com/wp-content/uploads/2017/08/EDEN-HALL-EXTERIOR-2017-012.jpg" alt="project-1"  width="360px">
                                    <div class="project-item-overlay">
                                        <h4>Eden Hall Day Spa</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 isotope-item marketing">
                        <a href="/friends/huxleys">
                            <div class="project-item">
                                <div class="overlay-container">
                                    <img src="http://www.dialhousehotel.com/wp-content/uploads/2017/08/Huxleys-May17-PRINTres-1.jpg" alt="project-1" heigth="360px" width="360px">
                                    <div class="project-item-overlay">
                                        <h4>Huxleys</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                </div>

            </div>
                </div>

            </div>

@endsection     