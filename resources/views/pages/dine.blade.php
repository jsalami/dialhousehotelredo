@extends('main')
@section('title', 'Dine')
@section('slide-image')

    <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/08/DSC_6430.jpg)"></div>
    <div class="swiper-slide" style="background-image:url(http://www.dialhousehotel.com/wp-content/uploads/2017/08/DSC_6088.jpg)"></div>

@endsection
@section('content')
    
    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12 image-grids">
                    <div class="text-center">
                       <center><img src="{{asset('images/logo_small.png')}}" class="img-responsive" alt='Heading Image' width='100px' /></center>
                        <h3>Dine</h3>
                        <div class="separator"></div>
                        <p>We have recently refurbished our dining area, with two very comfy, cosy, relaxed dining rooms. Come and experience our new menu with traditional English fayre and a twist of the Mediterranean. We also serve breakfast and Sunday Lunch for both hotel guests and those who are in the village.  If you are just looking for a place to have a drink you can sit in our new lounge area for a coffee, cocktail or locally brewed ale.</p>
                    </div>
                    <div class="col-md-6 text-center">
                        <button type="submit" class="button button-simple mt-30">View Food Menu</button>
                    </div>
                    <div class="col-md-6 text-center">
                        <button type="submit" class="button button-simple mt-30">View Drinks Menu</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    @include('partials._reservation-enquiry')
    @include('partials._openingtimes')
    
@endsection     