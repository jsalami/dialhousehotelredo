/*------------------------------------
    Google Maps
--------------------------------------*/ 
$(document).ready(function() {

    $('body').noisy({
        intensity: 0.2,
        size: 200,
        opacity: 0.28,
        randomColors: false, // true by default
        color: '#000000'
    });

    //Google Maps JS
    //Set Map
    function initialize() {
        var myLatlng = new google.maps.LatLng(51.884755, -1.756688);
        var imagePath = 'https://mt.googleapis.com/vt/icon/name=icons/onion/SHARED-mymaps-pin-container_4x.png,icons/onion/1899-blank-shape_pin_4x.png&highlight=000000&scale=2.0'
        var mapOptions = {
            zoom: 11,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);
        //Callout Content
        var contentString = 'The Dial House Hotel,<br> High Street,<br> Bourton on the Water,<br> GL54 2AN';
        //Set window width + content
        var infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 500
        });

        //Add Marker
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            icon: imagePath,
            title: 'image title'
        });

        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
        });

        //Resize Function
        google.maps.event.addDomListener(window, "resize", function() {
            var center = map.getCenter();
            google.maps.event.trigger(map, "resize");
            map.setCenter(center);
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

});

/*------------------------------------
    Preloader
--------------------------------------*/ 

  jQuery('#preloader').fadeOut('normall', function() {
      jQuery(this).remove();
  });


/*------------------------------------
    Scroll To Top
--------------------------------------*/ 

    jQuery(window).scroll(function(){
        if(jQuery(this).scrollTop() > 500) {
            jQuery(".scroll-to-top").fadeIn(400);
            
        } else {
            jQuery(".scroll-to-top").fadeOut(400);
        }
    });
 
    jQuery(".scroll-to-top").click(function(event){
        event.preventDefault();
        jQuery("html, body").animate({scrollTop: 0},600);
    });



/*------------------------------------
    Hover Drop Down
--------------------------------------*/    

if (jQuery(window).width() > 767) {
  jQuery('ul.nav li.dropdown').hover(function() {
      jQuery(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(300);
  }, function() {
      jQuery(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(300);
  });
}


/*------------------------------------
    Slider
--------------------------------------*/  
$(document).ready(function(){
var swiper = new Swiper('.swiper-container', {
  spaceBetween: 30,
  effect: 'fade',
  centeredSlides: true,
  autoplay: {
    delay: 2500,
    disableOnInteraction: false,
  },
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
});
});

/*------------------------------------
    Navbar
--------------------------------------*/  
$(document).ready(function(){
$(".closing-menu-button").click(function(){
    $("#js-bootstrap-offcanvas").trigger("offcanvas.toggle");
});
});

/*------------------------------------
    AJAX Form
--------------------------------------*/  
$("form.wpcf7-form").submit(function(e){
    e.preventDefault();
    var token = $("input[name=_token]").val(); // The CSRF token
    var first_name = $("input[name=first-name]").val();
    var last_name = $("input[name=last-name]").val();
    var email = $("input[name=email]").val();
    var phone = $("input[name=phone]").val();
    var bodyMessage = $("textarea[name=message]").val();
    var recaptcha_token = $("textarea[name=g-recaptcha-response]").val();

    $.ajax({
       type:'POST',
       url:'/contact',
       dataType: 'json',
       data:{_token: token, first_name:first_name, last_name:last_name, email:email, phone:phone, bodyMessage:bodyMessage, recaptcha_token:recaptcha_token},
       success:function(data){
           $(".email-success-messge").append(data.success).fadeIn(999);
           $("form.wpcf7-form").addClass(data.hide);
       }
    });
});

$("form.reservation-form").submit(function(e){
    e.preventDefault();
    var token = $("input[name=_token]").val(); // The CSRF token
    var fullname = $("input[name=fullname]").val();
    var phone_no = $("input[name=phone_no]").val();
    var email_add = $("input[name=email_add]").val();
    var number_of_guests = $("select[name=number_of_guests]").val();
    var date_of_reservation = $("input[name=date]").val();
    var time_of_reservation = $("input[name=time]").val();
    var recaptcha_token = $("#g-recaptcha-response").val();
    
    $.ajax({
       type:'POST',
       url:'/dine',
       dataType: 'json',
       data:{_token: token, fullname:fullname, phone_no:phone_no, email_add:email_add, number_of_guests:number_of_guests, date_of_reservation:date_of_reservation, time_of_reservation:time_of_reservation, recaptcha_token:recaptcha_token},
       success:function(data){
           $(".email-success-message").append(data.success).fadeIn(999);
           $("form.reservation-form").addClass(data.hide);
       }
    });
});