<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});

Route::get('/dine', function () {
    return view('pages.dine');
});

Route::get('/rooms', function () {
    return view('pages.rooms');
});

Route::get('/bookings', function () {
    return view('pages.bookings');
});

Route::get('/weddings', function () {
    return view('pages.weddings');
});

Route::get('/events', function () {
    return view('pages.events');
});

Route::get('/friends', function () {
    return view('pages.friends');
});

Route::get('/contact', function () {
    return view('pages.contact');
});

Route::get('/friends/eden-hall', function () {
    return view('pages.friends.eden-hall');
});

Route::get('/friends/hoarcrosshall', function () {
    return view('pages.friends.hoarcrosshall');
});

Route::get('/friends/huxleys', function () {
    return view('pages.friends.huxleys');
});

Route::get('/events/valentines-offer', function () {
    return view('pages.events.valentines-offer');
});

Route::resource('posts','PostController');

Route::post('/contact', 'ContactController@PostContact');

Route::post('/dine', 'ReservationController@postReservation');

Route::get('/sliders', 'SliderController@index');
Route::get('/sliders/create', 'SliderController@create');