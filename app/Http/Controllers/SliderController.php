<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function index(){
        return view('sliders.index');
    }
    
    public function create(){
        return view('sliders.create');
    }
}
