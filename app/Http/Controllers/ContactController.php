<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Mail;
use Session;

class ContactController extends Controller
{
    public function postContact(Request $request){
        $this->validate($request, ['email' => 'required|email'] );

        $data = array(
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'bodyMessage' => $request->bodyMessage,
            'recaptcha_token' => $request->recaptcha_token
        );
        
        if ($data['recaptcha_token']){
            Mail::send('emails.contact', $data, function($message) use ($data){
                $message->from($data['email']);
                $message->to('jafar@calmcollective.co.uk');
                $message->subject('Contact Details');
            });
            return response()->json(['success' => '<p class="alert alert-success">Thank you for getting in touch!</p>', 'hide' => 'display-none'], 200);
        }else{
            return response()->json(['success' => '<p class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> Please check the ReCaptcha box.</p>'], 200);
        }
    }
}