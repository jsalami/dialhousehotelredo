<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Mail;
use Session;

class ReservationController extends Controller
{
    public function postReservation(Request $request){

        $data = array(
            'fullname' => $request->fullname,
            'phone_no' => $request->phone_no,
            'email_add' => $request->email_add,
            'number_of_guests' => $request->number_of_guests,
            'date_of_reservation' => $request->date_of_reservation,
            'time_of_reservation' => $request->time_of_reservation,
            'recaptcha_token' => $request->recaptcha_token
        );
        
        if ($data['recaptcha_token']){
            Mail::send('emails.reservation', $data, function($messages) use ($data){
                $messages->from($data['email_add']);
                $messages->to('jafar@calmcollective.co.uk');
                $messages->subject('Reservation Details');
            });
            return response()->json(['success' => '<p class="alert alert-success">Thank you for getting in touch!</p>', 'hide' => 'display-none'], 200);
            
        }else{
            return response()->json(['success' => '<p class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> Please check the ReCaptcha box.</p>'], 200);
        }
    }
}